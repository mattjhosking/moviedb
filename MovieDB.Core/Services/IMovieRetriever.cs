﻿using System.Threading.Tasks;
using MovieDB.Core.DTOs;

namespace MovieDB.Core.Services
{
    public interface IMovieRetriever
    {
        Task<Movie[]> RetrieveMovies();
        Task<Actor[]> RetrieveActors();
    }
}