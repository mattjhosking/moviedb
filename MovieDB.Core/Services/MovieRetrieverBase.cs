﻿using System;
using System.Linq;
using System.Threading.Tasks;
using MovieDB.Core.DTOs;

namespace MovieDB.Core.Services
{
    public abstract class MovieRetrieverBase : IMovieRetriever
    {
        public abstract Task<Movie[]> RetrieveMovies();

        public async Task<Actor[]> RetrieveActors()
        {
            var movies = await RetrieveMovies();
            return movies
                ?.Where(movie => movie.Roles != null)
                .SelectMany(movie => movie.Roles
                    .Where(role => !string.IsNullOrEmpty(role.Actor))
                    .Select(role => new { Movie = movie.Name, Role = role.Name, role.Actor })
                )
                .GroupBy(x => x.Actor, StringComparer.OrdinalIgnoreCase)
                .Select(x => new Actor
                {
                    Name = x.Key,
                    Characters = x
                        .Select(y => new { y.Role, y.Movie })
                        .Distinct()
                        .Select(y => new Character { Name = y.Role, Movie = y.Movie }).ToArray()
                }).ToArray();
        }
    }
}