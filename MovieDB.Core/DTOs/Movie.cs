namespace MovieDB.Core.DTOs
{
    public class Movie
    {
        public string Name { get; set; }
        public Role[] Roles { get; set; }
    }
}