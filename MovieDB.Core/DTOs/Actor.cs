namespace MovieDB.Core.DTOs
{
    public class Actor
    {
        public string Name { get; set; }
        public Character[] Characters { get; set; }
    }
}