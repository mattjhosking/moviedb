namespace MovieDB.Core.DTOs
{
    public class Role
    {
        public string Name { get; set; }
        public string Actor { get; set; }
    }
}