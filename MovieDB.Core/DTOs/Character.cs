namespace MovieDB.Core.DTOs
{
    public class Character
    {
        public string Name { get; set; }
        public string Movie { get; set; }
    }
}