import { NgModule, APP_INITIALIZER  } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppModuleShared } from './app.module.shared';
import { AppComponent } from './components/app/app.component';
import { ConfigurationService } from "./services/configuration.service";
import { MoviesApiService } from "./services/moviesapi.base.service";
import { MoviesApiLiveService } from "./services/moviesapi.live.service";

@NgModule({
    bootstrap: [ AppComponent ],
    imports: [
        BrowserModule,
        AppModuleShared
    ],
    providers: [
        { provide: 'BASE_URL', useFactory: getBaseUrl },
        {
            provide: APP_INITIALIZER,
            useFactory: (configurationService: ConfigurationService) => () => configurationService.getConfiguration(),
            deps: [ConfigurationService],
            multi: true
        },
        { provide: MoviesApiService, useClass: MoviesApiLiveService },
        ConfigurationService
    ]
})
export class AppModule {
}

export function getBaseUrl() {
    return document.getElementsByTagName('base')[0].href;
}
