import { Component } from '@angular/core';
import { Observable } from "rxjs/Rx";
import 'rxjs/add/operator/shareReplay';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { MoviesApiService } from "../../services/moviesapi.base.service";
import { IActor } from "../../services/actor.interface";

@Component({
    selector: 'actors',
    templateUrl: './actors.component.html'
})
export class ActorsComponent {

    public actors: Observable<IActor[] | Object>;
    public useProxy: boolean = true;
    public useServerSideTransformation: boolean;
    public error: any;

    constructor(private moviesApiService: MoviesApiService) {
    }

    public retrieveActors() {
        this.error = null;
        this.actors = this.moviesApiService.getActors(this.useProxy, this.useProxy && this.useServerSideTransformation)
            .map(actors => actors || <IActor[]>[])
            .catch(err => {
                this.error = err;
                console.log(err);
                return Observable.of(new Object());
            })
            .shareReplay();
    }
}
