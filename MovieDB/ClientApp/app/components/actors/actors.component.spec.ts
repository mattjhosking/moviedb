/// <reference path="../../../../node_modules/@types/jasmine/index.d.ts" />
import { assert } from 'chai';
import { FormsModule } from '@angular/forms';
import { ActorsComponent } from './actors.component';
import { TestBed, async, ComponentFixture } from '@angular/core/testing';
import { MoviesApiService } from "../../services/moviesapi.base.service";
import { MoviesApiTestService } from "../../services/moviesapi.test.service";
import { OrderPipe } from "../../pipes/orderby.pipe";

let fixture: ComponentFixture<ActorsComponent>;
let moviesApiTestService: MoviesApiTestService;

describe('Actors component', () => {
    beforeEach(() => {
        moviesApiTestService = new MoviesApiTestService();
        TestBed.configureTestingModule({
            declarations: [
                ActorsComponent,
                OrderPipe
            ],
            imports: [
                FormsModule
            ],
            providers: [
                { provide: MoviesApiService, useValue: moviesApiTestService,  }
            ]
        });
        fixture = TestBed.createComponent(ActorsComponent);
        fixture.detectChanges();
    });

    it('should display a title', async(() => {
        const titleText = fixture.nativeElement.querySelector('h1').textContent;
        expect(titleText).toEqual('Actors');
    }));

    it('should start with no results retrieved displayed', async(() => {
        const resultsParagraph = fixture.nativeElement.querySelector('#results > p');
        expect(resultsParagraph).toBeTruthy('Couldn\'t find the results paragraph!');
        expect(resultsParagraph.textContent).toEqual('No data has been retrieved.');
    }));

    it('should display no results returned when no data provided', async(() => {
        fixture.componentInstance.retrieveActors();
        fixture.detectChanges();
        const resultsParagraph = fixture.nativeElement.querySelector('#results > p');
        expect(resultsParagraph).toBeTruthy('Couldn\'t find the results paragraph!');
        expect(resultsParagraph.textContent).toEqual('No results were returned.');
    }));

    it('should ignore blank actors', async(() => {
        moviesApiTestService.setMovies([{ name: 'Test Movie', roles: [{ name: 'Test Character', actor: '' }] }]);
        fixture.componentInstance.retrieveActors();
        fixture.detectChanges();
        const resultsParagraph = fixture.nativeElement.querySelector('#results > p');
        expect(resultsParagraph).toBeTruthy('Couldn\'t find the results paragraph!');
        expect(resultsParagraph.textContent).toEqual('No results were returned.');
    }));

    it('should display a result returned when a movie is provided', async(() => {
        moviesApiTestService.setMovies([ { name: 'Test Movie', roles: [ { name: 'Test Character', actor: 'Test Actor' } ] }]);
        fixture.componentInstance.retrieveActors();
        fixture.detectChanges();
        const resultsList = fixture.nativeElement.querySelector('#results > ul');
        expect(resultsList).toBeTruthy('Couldn\'t find the results list!');
        var listItems = resultsList.children;
        expect(listItems.length).toEqual(1);
        expect(listItems[0].firstChild.textContent.trim()).toEqual('Test Actor');
        const charactersList = listItems[0].querySelector('ul');
        expect(charactersList).toBeTruthy('Couldn\'t find the characters list!');
        var characters = charactersList.children;
        expect(characters.length).toEqual(1);
        expect(characters[0].firstChild.textContent.trim()).toEqual('Test Character');
    }));

    it('should ignore duplicate roles', async(() => {
        moviesApiTestService.setMovies([{ name: 'Test Movie', roles: [{ name: 'Test Character', actor: 'Test Actor' }, { name: 'Test Character', actor: 'Test Actor' }] }]);
        fixture.componentInstance.retrieveActors();
        fixture.detectChanges();
        const resultsList = fixture.nativeElement.querySelector('#results > ul');
        expect(resultsList).toBeTruthy('Couldn\'t find the results list!');
        var listItems = resultsList.children;
        expect(listItems.length).toEqual(1);
        expect(listItems[0].firstChild.textContent.trim()).toEqual('Test Actor');
        const charactersList = listItems[0].querySelector('ul');
        expect(charactersList).toBeTruthy('Couldn\'t find the characters list!');
        var characters = charactersList.children;
        expect(characters.length).toEqual(1);
        expect(characters[0].firstChild.textContent.trim()).toEqual('Test Character');
    }));

    it('should handle errors', async(() => {
        moviesApiTestService.setError("Manually failed retrieval.");
        fixture.componentInstance.retrieveActors();
        fixture.detectChanges();
        const errorDiv = fixture.nativeElement.querySelector('#results > div');
        expect(errorDiv).toBeTruthy('Couldn\'t find the error div!');
        const errorParagraph = errorDiv.querySelector('p');
        expect(errorParagraph).toBeTruthy('Couldn\'t find the error paragraph!');
        expect(errorParagraph.textContent).toEqual('The following error occurred while retrieving data:');
        const errorPre = errorDiv.querySelector('pre');
        expect(errorPre).toBeTruthy('Couldn\'t find the error pre!');
        expect(errorPre.textContent).toEqual('Manually failed retrieval.');
    }));
});
