import { Observable } from "rxjs/Rx";
import { IActor } from "./actor.interface";
import { IMovie } from "./movie.interface";

export abstract class MoviesApiService {
    abstract getActors(useProxy: boolean, useServerSideTransformation: boolean): Observable<IActor[]>;

    protected groupMoviesByActor(movies: IMovie[]): IActor[] {
        var actors: IActor[] = [];
        if (movies) {
            var actorLookup: any = {};
            for (let movie of movies) {
                for (let role of movie.roles) {
                    if (role.actor) {
                        if (!actorLookup.hasOwnProperty(role.actor)) {
                            var newActor: IActor = { name: role.actor, characters: [] };
                            actorLookup[role.actor] = newActor;
                            actors.push(newActor);
                        }
                        var actor: IActor = actorLookup[role.actor];
                        if (!actor.characters.some(c => c.name === role.name))
                            actor.characters.push({ name: role.name, movie: movie.name });
                    }
                }
            }
        }
        return actors;
    }
}
