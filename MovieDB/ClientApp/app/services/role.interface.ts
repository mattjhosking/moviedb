export interface IRole {
    name: string;
    actor: string;
}