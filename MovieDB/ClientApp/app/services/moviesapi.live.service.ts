import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from "rxjs/Rx";
import "rxjs/add/operator/map";
import { ConfigurationService } from "./configuration.service";
import { MoviesApiService } from "./moviesapi.base.service";
import { IActor } from "./actor.interface";
import { IMovie } from "./movie.interface";

@Injectable()
export class MoviesApiLiveService extends MoviesApiService {
    constructor(private http: Http, private configurationService: ConfigurationService) {
        super();
    }

    public getActors(useProxy: boolean, useServerSideTransformation: boolean): Observable<IActor[]> {
        if (useProxy && useServerSideTransformation) {
            return this.http.get(this.configurationService.configuration.localResourceServer + '/api/Movies/actors')
                .map((response: Response) => response.json() as IActor[]);
        }

        return (useProxy ? this.getMoviesViaProxy() : this.getMoviesDirect())
            .map((movies: IMovie[]) => this.groupMoviesByActor(movies));
    }

    private getMoviesDirect(): Observable<IMovie[]> {
        return this.getMoviesFromResourceServer(this.configurationService.configuration.remoteResourceServer);
    }

    private getMoviesViaProxy(): Observable<IMovie[]> {
        return this.getMoviesFromResourceServer(this.configurationService.configuration.localResourceServer);
    }

    private getMoviesFromResourceServer(resourceServer: string): Observable<IMovie[]> {
        return this.http.get(resourceServer + '/api/Movies').map((response: Response) => response.json() as IMovie[]);
    }
}