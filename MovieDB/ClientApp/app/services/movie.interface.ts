import { IRole } from "./role.interface";

export interface IMovie {
    name: string;
    roles: IRole[];
}