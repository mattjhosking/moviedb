import { ICharacter } from "./character.interface";

export interface IActor {
    name: string;
    characters: ICharacter[];
}