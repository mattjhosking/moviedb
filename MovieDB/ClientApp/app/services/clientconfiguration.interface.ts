export interface IClientConfiguration {
    localResourceServer: string;
    remoteResourceServer: string;
}