import { Injectable, Inject } from '@angular/core';
import { Http, Response } from '@angular/http';
import "rxjs/add/operator/toPromise";
import { IClientConfiguration } from "./clientconfiguration.interface";

@Injectable()
export class ConfigurationService {
    public configuration: IClientConfiguration;

    constructor(private http: Http, @Inject('BASE_URL') private baseUrl: string) {
    }

    public getConfiguration(): Promise<IClientConfiguration> {
        return this.http.get(this.baseUrl + 'api/Configuration').toPromise().then((response: Response) => this.configuration = response.json() as IClientConfiguration);
    }
}
