import { Injectable } from '@angular/core';
import { Observable } from "rxjs/Rx";
import "rxjs/add/operator/map";
import { MoviesApiService } from "./moviesapi.base.service";
import { IActor } from "./actor.interface";
import { IMovie } from "./movie.interface";

@Injectable()
export class MoviesApiTestService extends MoviesApiService {
    private movies: IMovie[];
    private errorText: string;

    constructor() {
        super();
    }

    public setMovies(movies: IMovie[]) {
        this.movies = movies;
    }

    public setError(errorText: string) {
        this.errorText = errorText;
    }

    public getActors(useProxy: boolean, useServerSideTransformation: boolean): Observable<IActor[]> {
        if (this.errorText)
            return Observable.throw(this.errorText);

        return Observable.of(this.groupMoviesByActor(this.movies));
    }

}