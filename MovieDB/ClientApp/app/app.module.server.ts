import { APP_INITIALIZER, NgModule } from '@angular/core';
import { ServerModule } from '@angular/platform-server';
import { AppModuleShared } from './app.module.shared';
import { AppComponent } from './components/app/app.component';
import { ConfigurationService } from "./services/configuration.service";
import { MoviesApiService } from "./services/moviesapi.base.service";
import { MoviesApiLiveService } from "./services/moviesapi.live.service";

@NgModule({
    bootstrap: [ AppComponent ],
    imports: [
        ServerModule,
        AppModuleShared
    ],
    providers: [
        {
            provide: APP_INITIALIZER,
            useFactory: (configurationService: ConfigurationService) => () => configurationService.getConfiguration(),
            deps: [ConfigurationService],
            multi: true
        },
        { provide: MoviesApiService, useClass: MoviesApiLiveService },
        ConfigurationService
    ]
})
export class AppModule {
}
