﻿import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'orderBy'
})
export class OrderPipe implements PipeTransform {

    transform(value: any | any[], property: string, ascending: boolean = true): any {
        if (!value) {
            return value;
        }

        let array: any[] = value.sort((a: any, b: any): number => {
            if (a && b) {
                return a[property] > b[property] ? 1 : -1;
            } else {
                return a > b ? 1 : -1;
            }
        });

        if (!ascending) {
            return array.reverse();
        }

        return array;
    }
}