using System;
using Microsoft.ApplicationInsights.AspNetCore.Extensions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using MovieDB.DTOs;

namespace MovieDB.Controllers
{
    [Route("api/[controller]")]
    [AllowAnonymous]
    public class ConfigurationController : Controller
    {
        public const string RemoteResourceServer = "http://alintacodingtest.azurewebsites.net";

        [HttpGet("")]
        [Produces(typeof(ClientConfiguration))]
        public IActionResult Get()
        {
            return Ok(new ClientConfiguration
            {
                LocalResourceServer = Request.GetUri().GetComponents(UriComponents.SchemeAndServer, UriFormat.Unescaped),
                RemoteResourceServer = RemoteResourceServer
            });
        }
    }
}
