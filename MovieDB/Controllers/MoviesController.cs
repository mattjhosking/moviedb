using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using MovieDB.Core.DTOs;
using MovieDB.Core.Services;

namespace MovieDB.Controllers
{
    /// <inheritdoc />
    /// <summary>
    /// Proxy controller required to access movies API on remote resource server as CORS is not enabled.
    /// Thus, direct AJAX calls to that server will fail without this proxy.
    /// </summary>
    [Route("api/[controller]")]
    [AllowAnonymous]
    public class MoviesController : Controller
    {
        private readonly IMovieRetriever _movieRetriever;

        public MoviesController(IMovieRetriever movieRetriever)
        {
            _movieRetriever = movieRetriever;
        }

        /// <summary>
        /// Retrieves a list of all movies with role names and actors
        /// </summary>
        /// <returns>An array of <see cref="Movie"/></returns>
        [HttpGet("")]
        [Produces(typeof(Movie[]))]
        public async Task<IActionResult> GetAll()
        {
            try
            {
                var movies = await _movieRetriever.RetrieveMovies();
                if (movies == null)
                    return NoContent();
                return Ok(movies);
            }
            catch (Exception exception)
            {
                return BadRequest(exception);
            }
        }

        /// <summary>
        /// Retrieve actors with server-side transformation from movies
        /// </summary>
        /// <returns>An array of <see cref="Actor"/></returns>
        [HttpGet("actors")]
        [Produces(typeof(Actor[]))]
        public async Task<IActionResult> GetAllActors()
        {
            try
            {
                var actors = await _movieRetriever.RetrieveActors();
                if (actors == null)
                    return NoContent();
                return Ok(actors);
            }
            catch (Exception exception)
            {
                return BadRequest(exception);
            }
        }
    }
}
