﻿using System;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using MovieDB.Controllers;
using MovieDB.Core.DTOs;
using MovieDB.Core.Services;
using Newtonsoft.Json;

namespace MovieDB.Services
{
    public class ApiMovieRetriever : MovieRetrieverBase
    {
        public override async Task<Movie[]> RetrieveMovies()
        {
            return await GetFromRemoteResourceServer<Movie[]>("/api/movies");
        }

        /// <summary>
        /// Retrieves data of the specified type from the remote resource server.
        /// </summary>
        /// <typeparam name="T">The data type to be deserialized from the JSON response.</typeparam>
        /// <param name="url">The path to the resource on the remote resource server.</param>
        /// <returns>An instance of type T.</returns>
        private static async Task<T> GetFromRemoteResourceServer<T>(string url)
        {
            using (var client = new HttpClient { BaseAddress = new Uri(ConfigurationController.RemoteResourceServer) })
            {
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                HttpResponseMessage response = await client.GetAsync(url);
                string content = await response.Content.ReadAsStringAsync();
                using (TextReader textReader = new StringReader(content))
                using (JsonReader jsonReader = new JsonTextReader(textReader))
                    return JsonSerializer.Create().Deserialize<T>(jsonReader);
            }
        }
    }
}
