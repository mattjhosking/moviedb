namespace MovieDB.DTOs
{
    public class ClientConfiguration
    {
        public string LocalResourceServer { get; set; }
        public string RemoteResourceServer { get; set; }
    }
}