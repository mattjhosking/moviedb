# MovieDB #

The MovieDB web app retrieves data from the Alinta movies API and displays information about actors.

## Getting Started ##

Shapify requires the following environment to build:

* Visual Studio 2017
* .NET Core 2.0
* Node.js

To build, simply load the solution in Visual Studio and rebuild all.

To run, execute as normal and the main page will be displayed.

## Troubleshooting ##

In some environments (depending on configuration), Visual Studio may fail to build the webpack bundle automatically. To build it manually, ensure webpack is installed globally (i.e. npm install webpack -g) and run:

* webpack --config webpack.config.vendor.js
* webpack

## Running Tests ##

### Front End ###

The tests require that karma is globally installed (i.e. npm install karma -g). To run the tests, open a console, browse to the project folder and run 'npm test'. A browser window will be opened to run the tests and results will be shown in the console window.

### Back End ###

Back end tests are written in xUnit and can be executed using the Visual Studio test runner. All tests pass and will show green ticks after execution.

## Instructions ##

Upon loading the web app, navigate to the "Actors" page. From there you can specify two settings for retrieving the actor information:

1. Retrieve data via local proxy service (required if CORS is not enabled for this site on the remote server).
2. Group movie data by actors on the proxy server (instead of in the browser).

The first option is required to be set to true where CORS has not been enabled to allow direct API access from web browser AJAX calls (which was the case at time of writing).
The second option is available when using the local proxy service and performs the grouping of data into the actor information on the server instead of using local javascript processing in the web browser.

### Error Handling ###
The following scenarios are handled:

* No data returned (no content response / empty array) - "No results were returned." is displayed.
* Roles without actor names - these are ignored.
* Duplicated roles for a movie - only distinct roles are returned.
* Errors retrieving the data (non 2xx responses) - "The following error occurred while retrieving data:" is displayed with error details.

## Features ##
### Front End ###
* Angular 4.3.6
* Bootstrap 3 look and feel
* Retrieves client configuration on Angular start up from back end
* AJAX call to server to retrieve information
* Asynchronous binding to observable with shared response (single call for multiple usages of response)
* Sorting on actors and characters performed by Angular pipe
* Strongly typed web service data on client through interfaces
* Retrieval functionality moved to services and consumed by components
* Web pack used for bundling with split server / client
* Instructions provided on home page
* Simple interface and operation

### Back End  ###
* .NET Core 2.0 for back end
* MVC controllers for retrieving client configuration and server-side functionality
* Strongly typed data retrieved and returned from REST API
* Server side pre-rendering of Angular content
