using System.Threading.Tasks;
using MovieDB.Core.DTOs;
using MovieDB.Tests.Services;
using Xunit;

namespace MovieDB.Tests.MovieRetrieverBaseTests
{
    public class RetrieveActors
    {
        [Fact]
        public async Task NullMovieResultIsHandled()
        {
            Actor[] result = await new TestMovieRetriever(null).RetrieveActors();
            Assert.Null(result);
        }

        [Fact]
        public async Task RolesWithoutActorsAreIgnored()
        {
            Actor[] result = await new TestMovieRetriever(new[]
            {
                new Movie
                {
                    Name = "Test Movie",
                    Roles = new[]
                    {
                        new Role
                        {
                            Name = "Test Character",
                            Actor = ""
                        }
                    }
                }
            }).RetrieveActors();
            Assert.Empty(result);
        }

        [Fact]
        public async Task EmptyMoviesAreHandled()
        {
            Actor[] result = await new TestMovieRetriever(new[]
            {
                new Movie
                {
                    Name = "Test Movie"
                }
            }).RetrieveActors();
            Assert.Empty(result);
        }

        [Fact]
        public async Task SimpleMovieResult()
        {
            Actor[] result = await new TestMovieRetriever(new[]
            {
                new Movie
                {
                    Name = "Test Movie",
                    Roles = new[]
                    {
                        new Role
                        {
                            Name = "Test Character",
                            Actor = "Test Actor"
                        }
                    }
                }
            }).RetrieveActors();
            Assert.Equal(1, result.Length);
            Assert.Equal("Test Actor", result[0].Name);
            Assert.Equal(1, result[0].Characters.Length);
            Assert.Equal("Test Character", result[0].Characters[0].Name);
            Assert.Equal("Test Movie", result[0].Characters[0].Movie);
        }

        [Fact]
        public async Task DuplicateRolesAreIgnored()
        {
            Actor[] result = await new TestMovieRetriever(new[]
            {
                new Movie
                {
                    Name = "Test Movie",
                    Roles = new[]
                    {
                        new Role
                        {
                            Name = "Test Character",
                            Actor = "Test Actor"
                        },
                        new Role
                        {
                            Name = "Test Character",
                            Actor = "Test Actor"
                        }
                    }
                }
            }).RetrieveActors();
            Assert.Equal(1, result.Length);
            Assert.Equal("Test Actor", result[0].Name);
            Assert.Equal(1, result[0].Characters.Length);
            Assert.Equal("Test Character", result[0].Characters[0].Name);
            Assert.Equal("Test Movie", result[0].Characters[0].Movie);
        }

        [Fact]
        public async Task ActorsAreGroupedCorrectly()
        {
            Actor[] result = await new TestMovieRetriever(new[]
            {
                new Movie
                {
                    Name = "Test Movie",
                    Roles = new[]
                    {
                        new Role
                        {
                            Name = "Test Character",
                            Actor = "Test Actor"
                        }
                    }
                },
                new Movie
                {
                    Name = "Test Movie 2: The Sequel",
                    Roles = new[]
                    {
                        new Role
                        {
                            Name = "Test Character (reprising role)",
                            Actor = "test actor"
                        }
                    }
                }
            }).RetrieveActors();
            Assert.Equal(1, result.Length);
            Assert.Equal("Test Actor", result[0].Name);
            Assert.Equal(2, result[0].Characters.Length);
            Assert.Equal("Test Character", result[0].Characters[0].Name);
            Assert.Equal("Test Movie", result[0].Characters[0].Movie);
            Assert.Equal("Test Character (reprising role)", result[0].Characters[1].Name);
            Assert.Equal("Test Movie 2: The Sequel", result[0].Characters[1].Movie);
        }
    }
}
