using System.Threading.Tasks;
using MovieDB.Core.DTOs;
using MovieDB.Core.Services;

namespace MovieDB.Tests.Services
{
    public class TestMovieRetriever : MovieRetrieverBase
    {
        private readonly Movie[] _movies;

        public TestMovieRetriever(Movie[] movies)
        {
            _movies = movies;
        }

        public override Task<Movie[]> RetrieveMovies()
        {
            return Task.FromResult(_movies);
        }
    }
}